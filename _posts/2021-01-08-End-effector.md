---
layout: post
title: End Effector
category: [Projects]
tags: [robots, 3D printing]
---

Here's the end effector made out of carbon fiber polycarbonate, a couple of DYNAMIXEL servos, and various nuts/bolts.

<p align="center"><iframe id="player" width="640" height="480" src="//www.youtube.com/embed/0b93V4e2tE8?enablejsapi=1&origin=https://thomasfagan.info&showinfo=0&iv_load_policy=3&modestbranding=1&theme=light&color=white&rel=0" frameborder="0"></iframe></p>