---
layout: post
title: Blob detection
category: [Projects]
tags: [computer vision]
---

 After meeting with a lab professor, he suggested that a tool to quickly find changes in engineering drawings would be useful. So, I threw together this little demo over the weekend.

<p align="center"><iframe id="player" width="640" height="480" src="//www.youtube.com/embed/eeQKFrIUVWA?enablejsapi=1&origin=https://thomasfagan.info&showinfo=0&iv_load_policy=3&modestbranding=1&theme=light&color=white&rel=0" frameborder="0"></iframe></p>

The red squares represent the blobs of difference and, though completely unnecessary, they do add some spectacle to the whole thing.