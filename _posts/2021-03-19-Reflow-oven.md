---
layout: post
title: Reflow Oven
category: [Projects]
tags: [PCBs]
---

Here is this beautiful reflow oven. It allows you to heat your PCBS in a more controlled way. By following the heat profile suggested by the solder paste manufacturer, you reduce the number of failures and create a better contact between the parts and the board. It's just a very well insulated, solid-state-relay-controlled toaster oven.

I cut myself more times than I'd like to admit assembling it.

## Outside
<img src="/assets/images/reflow_oven_closed.jpg" style="width:100%">

## Inside
<img src="/assets/images/reflow_oven_open.jpg" style="width:100%">

## LCD
<img src="/assets/images/reflow_oven_display.jpg" style="width:100%">