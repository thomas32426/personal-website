---
layout: post
title: Visual Odometry using Convolutional Neural Networks
category: [Projects]
tags: [computer vision, neural networks, research]
---

After recently learning about the magic of deep learning, two of my friends and I got together with the fantastic professor McFall and shot big. CNNs seemed to solve all sorts of intractable problems in computer vision, so why not visual odometry?

For those of you who don't know what visual odometry is, it's when you try to figure out how the camera moved in space using only the images it took along the way. Traditionally, this is done by finding visual features that are common in two images and throwing some trig at the situation to find the difference in poses. So, why not find the visual features using a CNN? Why not let it figure out all of that trig stuff, too?

<img src="/assets/images/shitty_results.png" style="width:100%">

As you can see, that it didn't work all that well (the colored lines are supposed to look like the blue ones). The models might have overfit based upon the validation sequence results and they certainly didn't generalize based upon the test sequence results. We did get to make lots of neat graphs and tinker with Keras, though.

If you're interested in reading the [paper](https://digitalcommons.kennesaw.edu/kjur/vol5/iss3/5/), there you go.