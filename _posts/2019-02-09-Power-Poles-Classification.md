---
layout: post
title: Power Pole Classification
category: [Projects]
tags: [computer vision, neural networks]
---

A friend of ours had some contacts at a big civil engineering firm here in Georgia. He said that they might have need of a system that could identify and locate components of power poles from images. So, we spent a few months going through the process of creating that system. It involved getting the images, creating the labeling software, labeling the data, training and assessing several neural networks, and post processing the output of those networks into something that wasn't quite as silly. 

We also experimented with some height conversion stuff using some basic trig to figure out the actual height of the points, but didn't quite complete it (not super tricky, though).

In the end, it seemed pretty obvious that the company was attempting to exploit us, so we just walked away (even though they did offer us a decent amount of money). It was pretty depressing, but it was a good lesson.

## Here are some of the pretty pictures:

<img src="/assets/images/labeled_poles_04.jpg" style="width:100%">
<img src="/assets/images/labeled_poles_02.jpg" style="width:100%">
<img src="/assets/images/labeled_poles_03.jpg" style="width:100%">
<img src="/assets/images/labeled_poles_01.jpg" style="width:100%">


