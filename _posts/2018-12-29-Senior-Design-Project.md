---
layout: post
title: Senior Design Project
category: [Projects]
tags: [robot, Arduino, Bluetooth, haptic feedback]
---

I did a senior design thing. The objective of the project was to control a robot arm using a control glove utilizing a VR tracker. Using a blutooth glove, force feedback was also achieved. The loud buzzing sound is those little servos on the glove doing their best to signal that the robot gripper has clasped around an object.

<p align="center"><iframe id="player" width="640" height="480" src="//www.youtube.com/embed/Eeg4w9ah2tE?enablejsapi=1&origin=https://thomasfagan.info&showinfo=0&iv_load_policy=3&modestbranding=1&theme=light&color=white&rel=0" frameborder="0"></iframe></p>

<p align="center"><img src="/assets/images/glove_prototype.jpg" width="490" height="1008"/></p>

Here's a picture of the earlier (wired) glove prototype. We eventually added a Bluetooth module and a battery pack so everything was being transmitted wirelessly.

If you're interested in seeing some clips from the presentation, here are two longer ones:

<p align="center"><iframe id="player" width="640" height="480" src="//www.youtube.com/embed/UYpNFkQiaoM?enablejsapi=1&origin=https://thomasfagan.info&showinfo=0&iv_load_policy=3&modestbranding=1&theme=light&color=white&rel=0" frameborder="0"></iframe></p>

<p align="center"><iframe id="player" width="640" height="480" src="//www.youtube.com/embed/pr79h3YWnR4?enablejsapi=1&origin=https://thomasfagan.info&showinfo=0&iv_load_policy=3&modestbranding=1&theme=light&color=white&rel=0" frameborder="0"></iframe></p>

If you're interested in seeing the [code](https://github.com/thomas32426/haptic-kinova), there you go.