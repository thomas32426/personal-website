---
layout: post
title: Mujoco and PPO
category: [Projects]
tags: [reinforcement learning, mujoco]
---

I spent the week working with Matteo to learn all about deep reinforcement learning. We used PPO to try to get some things to walk. It kinda worked. It's kind of fun to watch, too.

<p align="center"><iframe id="player" width="640" height="480" src="//www.youtube.com/embed/h8SgNguVQn0?enablejsapi=1&origin=https://thomasfagan.info&showinfo=0&iv_load_policy=3&modestbranding=1&theme=light&color=white&rel=0" frameborder="0"></iframe></p>

This guy does a decent job, really. I'd call that walking.

<p align="center"><iframe id="player" width="640" height="480" src="//www.youtube.com/embed/ijrXqmGut3o?enablejsapi=1&origin=https://thomasfagan.info&showinfo=0&iv_load_policy=3&modestbranding=1&theme=light&color=white&rel=0" frameborder="0"></iframe></p>

Don't quit your day job, guy.