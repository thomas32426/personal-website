---
layout: post
title: Underwater Object Detection
category: [Projects]
tags: [computer vision, neural networks, research]
---

We had some extra time during the directed research, so we made this paper about fine tuning YOLOv2 for use in the [RoboSub](https://robonation.org/programs/robosub/) competition. As you can see, it worked pretty well.

<img src="/assets/images/yolov2_underwater.png" style="width:100%">

If you're interested in reading the [paper](https://www.researchgate.net/publication/323801313_Fine-Tuning_YOLO_to_Perform_a_Real-Time_Visual_Multi-object_Detection_Task_for_an_Autonomous_Underwater_Vehicle), there you go.