---
layout: post
title: Robot PCBs
category: [Projects]
tags: [robots, PCBs]
---

At some point, we started to realize that the 8-layer, $1500 board with serious supply issues wasn't a good thing to rely on. So, we set out to make a 2-layer board for the motor controller and the optical encoder designs. The optical encoder board was pretty easy to interpret from the schematic, but the motor controller board needed large revisions. Several elements were removed and a few little quirks were added.

Some of the major modifications were that the board would use modular Trinamic stepper motor drivers instead of the surface mount ones. This would mean you could easily swap them out if they failed and you'd be able to utilize the higher microstepping modes and the silent stteps of the Trinamic boards. Instead of trying to pack it all in between the first and second joints, we decided to elimiate the size constaints and just have more wires going to an external board.

At one point, we really messed up and swapped the places of the two FPGA connectors. That was a very depressing and difficult thing to troubleshoot. But, after redoing it, there was just one minor mistake (a short) that we could scratch off. Worked like a charm after that.

## Optical Encoder Schematics
<img src="/assets/images/optical_schematic.png" style="width:100%">

## Optical Encoder PCB Layout
<img src="/assets/images/optical_board.png" style="width:100%">

## Motor Controller Schematics
<img src="/assets/images/motor_controller_schematics.png" style="width:100%">

## Motor Controller Bare PCB (Front and Back)
<img src="/assets/images/bare_pcbs.jpg" style="width:100%">

## Motor Controller Bare PCB (In the Light)
<img src="/assets/images/dexter_pcb.jpg" style="width:100%">

## Populated Motor Controller Board
<img src="/assets/images/populated_pcb.jpg" style="width:100%">