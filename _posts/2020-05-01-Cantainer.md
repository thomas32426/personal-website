---
layout: post
title: Cantainer
category: [Projects]
tags: [computer vision, web development]
---

I guess this is going to be more of a story than anything else. One of the interns at Greenzie had worked with another start-up on an interesting project. Apparently, it's illegal to sell unopened beer cans at festivals. The intern's senior design project was to create a device that could open the can. His team succeeded at this, but they were having difficulty orienting the can so that the tab could be popped. I told him that an object detection neural network would be great at doing this. One of his teammates gave it a shot, but came up short. So, I got in contact with the founder. Over the weekend, I showed him how to take the pictures I needed and how to label the data. He did that. I trained a network and it worked flawlessly. By detecting the center point of the can, the mouth of the can, and the tab, the angles could all be calculated very easily and used to control a motor.

Here's the code for the [detector/visualizer](https://gitlab.com/thomas32426/vending-machine) if you're interested.

It turned out that going for the vending machine component probably wasn't the best short-term move; An app was the way to go. Since the founder didn't know anything about programming, I thought "screw it" and gave it a shot. He had created a pretty full-functional front-end through Adobe XD. I saw that I could convert it to React Native code and stitch it in pretty easily. I learned about how to set up navigation and made a little test app. It was pretty easy, actually.

In the end, everything kind of fell through. So, blah.

Here's the code for the  and the [app](https://gitlab.com/the-cantainer/cantainer-app) that I made if you're interested.